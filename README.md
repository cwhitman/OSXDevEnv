OS X Development Environment Setup Package
=====
###### This repository contains all resources and instructions necessary for installation and initialization of a local development environment on OS X 10.12 Sierra. ######

*Pease note that this repo does not contain detailed usage instructions on the bundled applications or utilities except where necessary for installation or setup purposes.*


## Table of Contents ##
---
##### 1. iTerm #####
##### 2. Sublime #####
##### 3. OhMyZsh #####
##### 4. Themes #####
##### 5. Homebrew #####
##### 6. Keka #####
##### 7. Xcode #####
##### 8. Composer #####
##### 9. Laravel #####
##### 10. Homestead #####

---
### iTerm ###
##### *Upgrade your CLI* #####

iTerm2 is a replacement for the traditional Terminal that comes bundled with OS X. It provides a more full-featured and user-friendly experience when working with the command line.

##### Installing iTerm #####

The best way to install the application is to copy and paste the following code snippet into terminal and run it.

    mkdir -p ~/.OSXDevEnvDLs && cd ~/.OSXDevEnvDLs && wget https://iterm2.com/downloads/stable/iTerm2-3_0_10.zip && tar -zxvf iTerm2-3_0_10.zip && rm iTerm2-3_0_10.zip && mv iTerm.app /Applications/iTerm.app && cd ~ && rm -rf ~/.OSXDevEnvDLs && cd -

Alternatively, you can go to the [iTerm website](https://www.iterm2.com/downloads.html) and download and install the application manually.

---
### Sublime ###
##### *The best text editor* #####

From the Sublime website:
> Sublime Text is a sophisticated text editor for code, markup and prose. You'll love the slick user interface, extraordinary features and amazing performance.

##### Installing Sublime #####

This repo includes Sublime Text 3, Build No. 3126. This is built for OS 10.7+.

The best way to install the application is to copy and paste the following code snippet into terminal and run it.

    mkdir -p ~/.OSXDevEnvDLs && cd ~/.OSXDevEnvDLs && wget https://download.sublimetext.com/Sublime%20Text%20Build%203126.dmg && hdiutil attach Sublime\ Text\ Build\ 3126.dmg && cp -R /Volumes/Sublime\ Text/Sublime\ Text.app /Applications/ && cd ~ && hdiutil detach disk2s1 && rm -rf ~/.OSXDevEnvDLs $$ cd -

Alternatively, you can go to the [Sublime website](https://www.sublimetext.com/3) and download and install the application manually.

Once Sublime is installed, we need to set up a few things. The first is to enable running it from the command line. We'll create a symlink to enable this. Run the following command from the command line:

    cd /usr/local/bin && ln -s "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl" subl && cd -

Now you can open sublime from the command line by typing `subl`. If you want to open sublime in your current directory simply type `subl .`. There are a number of other tricks and features that you can read up on in the Sublime documentation.

Now that we have that set, let's get Sublime looking a bit nicer. Open sublime by typing `subl` from the command line - it doesn't matter what directory you're in. Once the application opens press <kbd>Cmd</kbd> + <kbd>Shift</kbd> + <kbd>p</kbd>. This opens the *command palette*, which gives you direct access to the menu items and packages available for Sublime without the need to hunt via the mouse. With the Command Palette open, type in *Package Control* and select the installation of the utility. Once it's installed, open up the command palette again and type "Install Package". Once the list comes up, type in "Material Theme". Once the theme is downloaded and type in "Material Theme Activate" and select the entry to activate the theme. To install the Material Theme Appbar, open the Command Palette again, type "Install Package", search for "Material Theme Appbar", and hit <kbd>Enter</kbd> to install. Once both are installed and activated, restart Sublime.

---
### Homebrew ###
##### *Package Manager* #####

Homebrew is an awesome package manager for OS X, which honestly should be a native install for every user. But since it isn't you'll have to install it yourself. Easy though! Just copy and paste this to your terminal.

    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

It's big, so be patient, but that's all there is to installing it.

---
### Zsh & Oh My Zsh ###
##### *Framework for managing ZSH Config* #####

Your CLI is about to get WAY better. First, use Homebrew to update the version of Zsh you have installed: `brew install zsh`, then run `chsh -s /usr/local/bin/zsh` to change your default shell from bash to Zsh. We recommend restarting iTerm once you've run the commands.

Once finished, you can now install OhMyZsh. Feel free to check out the [OhMyZsh github page](https://github.com/robbyrussell/oh-my-zsh) if you want more info, but if you trust us just run the following command:

    cd ~ && sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" && cd -

Once installed you'll be good to go! We'll be making a few changes to the Zsh config file while we install a few additional components, so run the following command to open it up in Sublime:

    subl ~/.zshrc

The first extension we'll install is a syntax highlighting plugin. Run the following command:

    cd ~/.oh-my-zsh && git clone git://github.com/zsh-users/zsh-syntax-highlighting.git && cd -

and add the following line to the end of your Zsh config file you have open in Sublime:

    source ~/.oh-my-zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

Next we'll bring in a utility called Z that will make navigating via command line much faster. Install Z with brew:

    brew install z

and add the following line to the Zsh config file:

    . `brew --prefix`/etc/profile.d/z.sh

Then refresh/restart your terminal using

    source ~/.zshrc

---
### Themes ###
##### *Making things look better* #####

The standard appearance of both iTerm and Zsh is pretty lack-luster, and can slow down your workflow as well as cause eye strain. We've included our favourite theme, [Honukai by @oskar](https://github.com/oskarkrawczyk/honukai-iterm-zsh), but feel free to seek out a theme and style that works best for you.

###### Zsh Theme ######

To set the theme in Zsh run this command:

    mv honukai-iterm-zsh/honukai.zsh-theme ~/.oh-my-zsh/themes/ && cp fonts/*.ttf /Library/Fonts/

Then run the following command:

    subl ~/.zshrc

and edit the line that says "ZSH_THEME" to read

    ZSH_THEME="honukai"

Then run the following command to reload your ZSH shell:

    source ~/.zshrc

###### iTerm Theme ######

To set the theme for iTerm, open the application and press **Cmd + i**. This opens the preferences pane. Click on the "Colors" tab, click on the dropdown in the lower right corner, and select "Import". Navigate to the honukai-iterm-zsh folder in this repository and select the "honukai.itermcolors" file. There are some additional settings you should set as well:

**Text Tab**
* Set 'Cursor' to *Vertical*
* Disable *Draw bold in bold font*
* Disable *Draw bold in bright colors*
* Disable *Blinking text allowed*
* Set *Use thin strokes for anti-aliased text* to *Always*
* Change the *Font* to *Source Code Pro Regular*. Set the size and spacing to what you prefer.
* Select *Non-ASCII Font* and set it to *Source Code Pro Semibold*
* Enable *Anti-aliased* for both font selections.

**Window Tab**
* Set *Transparency* to *~10%*
* Set *Blur* to *~%50*



---
### Keka ###
##### *File Archive Utility* #####

Keka is a free, open-source file archive utility that gives the ability to interact with the most useful compression formats - 7z, Tar, and Gzip files - from the OS X GUI.

We recommend installing Keka using Homebrew using the following command:

    brew cask install keka

If there are any problems, or if you prefer doing things manually, you can download the Keka disk image [here](http://download.kekaosx.com/).

---
### Xcode ###

###### Downloading Xcode ######

To set up OS X as a local development environment you will need to ensure you have a few things in place. The first of these is Xcode, Apple's development bundle. We would recommend downloading this from the [Apple App Store](https://itunes.apple.com/us/app/xcode/id497799835?mt=12)

This will take a bit to install, so we suggest doing this while you can chill and work on something else. It's a big application, but it's worth the wait.

###### Developer CLI Tools ######

Next we'll need to install the XCode Developer CLI Tools. Do so by running the following command:

    xcode-select --install

You'll be presented with a software update dialogue box. Click install and wait.

Once everything is done, fire up XCode once and accept the license agreement, then close the application.

---
### Composer ###
##### *PHP Package Manager* #####

Composer is an awesome package manager and is very useful. To download and install, run the following script:

    mkdir -p ~/.OSXDevEnvTmp && cd ~/.OSXDevEnvTmp && wget https://raw.githubusercontent.com/composer/getcomposer.org/1b137f8bf6db3e79a38a5bc45324414a6b1f9df2/web/installer -O - -q | php -- --quiet && mv composer.phar /usr/local/bin/composer && cd - && rm -rf ~/.OSXDevEnvTmp

With Composer installed globally you can run `Composer` from anywhere.

---
### Laravel ###

Download and install laravel using Composer:

    composer global require "laravel/installer"

And add `export PATH=~/.composer/vendor/bin:$PATH` to the end of your .zshrc file (which can be opened via `subl ~/.zshrc`), then restart Zsh (`source ~/.zshrc`).

To create a new laravel project, all you need to do is run `laravel new [project]` where `[project]` is any name you choose for your new project.

---
### Homestead ###

To make local development easier we're going to set up Homestead, a local development server. This requires the installation of a few applications. You'll need to enter your password for these installs.

The first is VirtualBox.

    mkdir -p ~/.OSXDevEnvDLs && cd ~/.OSXDevEnvDLs && wget http://download.virtualbox.org/virtualbox/5.1.6/VirtualBox-5.1.6-110634-OSX.dmg && hdiutil attach VirtualBox-5.1.6-110634-OSX.dmg && sudo installer -pkg "/Volumes/VirtualBox/VirtualBox.pkg" -target / && cd ~ && hdiutil detach disk2s1 && rm -rf ~/.OSXDevEnvDLs $$ cd -

Next is Vagrant:

    mkdir -p ~/.OSXDevEnvDLs && cd ~/.OSXDevEnvDLs && wget https://releases.hashicorp.com/vagrant/1.8.6/vagrant_1.8.6.dmg && hdiutil attach vagrant_1.8.6.dmg && sudo installer -pkg "/Volumes/Vagrant/Vagrant.pkg" -target / && cd ~ && hdiutil detach disk2s1 && rm -rf ~/.OSXDevEnvDLs $$ cd -

Next add the laravel/homestead box to the Vagrant installation using `vagrant box add laravel/homestead`. This will take a few minutes.

Now install Homestead in your home directory. It is ideal to have the box live there since it will be the server to all your projects: 

    cd ~ && git clone https://github.com/laravel/homestead.git Homestead && bash init.sh && cd -

All set! Time to configure everything.

###### Homestead Configuration ######

Open up your Homestead Config file using `subl ~/.homestead/Homestead.yaml`. Set *provider* to *virtualbox*. This file is also where you edit the mapping for your local sites. Refer to the [Laravel documentation](https://laravel.com/docs/5.3/homestead) for details on setting this up.